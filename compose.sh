#!/bin/sh

if test -w /var/run/docker.sock; then
  COMPOSE="docker-compose"
else
  COMPOSE="sudo docker-compose"
fi

export BRANCH=$(git rev-parse --abbrev-ref HEAD)
export COMPOSE_DOCKER_CLI_BUILD=1
export DOCKER_BUILDKIT=1

$COMPOSE -p "cdo-server-$BRANCH" "$@"
