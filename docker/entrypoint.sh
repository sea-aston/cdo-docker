#!/bin/bash

CDO_HOME="$HOME/cdo-server"
CDO_CONFIG="$CDO_HOME/config/cdo-server.xml"

mkdir -p "$(dirname "$CDO_CONFIG")"
cat >"$CDO_CONFIG" <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<cdoServer>
  <acceptor type="tcp" listenAddr="0.0.0.0" port="${CDO_PORT:-2036}"/>

  <repository name="${CDO_REPO:-repo}">
    <property name="overrideUUID" value=""/>
    <property name="supportingAudits" value="${CDO_AUDITS:-false}"/>
    <property name="supportingBranches" value="${CDO_BRANCHES:-false}"/>
    <property name="supportingUnits" value="false"/>
    <property name="checkUnitMoves" value="false"/>
    <property name="ensureReferentialIntegrity" value="false"/>
    <property name="allowInterruptRunningQueries" value="true"/>
    <property name="idGenerationLocation" value="STORE"/> <!-- Possible values: STORE | CLIENT -->
    <property name="commitInfoStorage" value="WITH_MERGE_SOURCE"/> <!-- Possible values: NO | YES | WITH_MERGE_SOURCE -->
    <property name="serializeCommits" value="false"/>
    <property name="optimisticLockingTimeout" value="10000"/>

    <store type="db">
      <!-- Period at which to execute an SQL statement to keep DB connection alive, in minutes -->
      <property name="connectionKeepAlivePeriod" value="60"/>

      <!-- Maximum number of store accessors (JDBC connections) to keep in the reader pool. The default value is 15.  -->
      <property name="readerPoolCapacity" value="20"/>

      <!-- Maximum number of store accessors (JDBC connections) to keep in the writer pool. The default value is 15.  -->
      <property name="writerPoolCapacity" value="20"/>

      <mappingStrategy type="horizontal"> <!-- callout -->
        <property name="qualifiedNames" value="true"/>
        <property name="withRanges" value="false"/>
        <property name="eagerTableCreation" value="false"/>
      </mappingStrategy>

      <dbAdapter name="h2"/>
      <dataSource class="org.h2.jdbcx.JdbcDataSource"
	      URL="jdbc:h2:/home/user/cdo-server/h2/big;LOCK_TIMEOUT=10000;TRACE_LEVEL_FILE=0"/>
    </store>

  </repository>
</cdoServer>
EOF

cd $CDO_HOME
exec "$@"
