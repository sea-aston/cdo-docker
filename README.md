# Docker image for CDO

This is a Docker image for [Eclipse CDO](https://www.eclipse.org/cdo/), a model repository and runtime persistence framework for [EMF](https://www.eclipse.org/modeling/emf/)-based models.

The Docker image uses Maven Tycho to build its own copy of the CDO Server product straight from the official update sites, which is not distributed directly from the CDO website.
It does not use the recommended [Oomph-based installer](https://archive.eclipse.org/modeling/emf/cdo/drops/R20150916-0434/help/org.eclipse.emf.cdo.doc/html/operators/Doc00_OperatingServer.html) as the latest release is quite old (based on Oxygen).

## Using with Docker Compose

For the most basic form of operation, use something like this in your [Docker Compose](https://docs.docker.com/compose/install/) file:

```yaml
version: "2.4"

services:
  cdo:
    image: registry.gitlab.com/sea-aston/cdo-docker:latest
    environment:
      CDO_PORT: 2036
      CDO_REPO: repo
      CDO_AUDITS: "false"
      CDO_BRANCHES: "false"
    ports:
      - "2036:2036"
    tmpfs:
      - /tmp
    volumes:
      - repo_data:/home/user/cdo-server/h2
      - repo_config:/home/user/cdo-server/configuration

volumes:
  repo_data:
  repo_config:
```

The above fragment will pull the image directly from this project's Docker repository and run it.
If you would like to build the image yourself, clone this repository and run this command:

```sh
./compose.sh up
```

Either of the two options above will start a repository called `repo` using the H2 store, listening on port 2036.

## Building with plain Docker

In order to build the image with plain Docker, run this command:

```sh
docker build -t cdo-docker:latest .
```

## Configuration

The Docker image includes an entrypoint script which automatically generates the `cdo-server.xml` file for you.
The script supports the following environment variables:

* `CDO_PORT`: the port under which CDO should be listening, using its TCP connector.
* `CDO_REPO`: the name of the repository being hosted by CDO.
* `CDO_AUDITS`: `"true"` iff the server should support auditing (versioning), `"false"` otherwise.
* `CDO_BRANCHES`: `"true"` iff the server should support branching, `"false"` otherwise.

## Limitations

Currently, the entrypoint supports only using the default H2 embedded database and the TCP connector.
